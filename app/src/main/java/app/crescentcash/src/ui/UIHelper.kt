package app.crescentcash.src.ui

import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.ContactsContract
import androidx.annotation.UiThread
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import android.text.Html
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.SimpleAdapter
import android.widget.Spinner
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast

import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.luminiasoft.ethereum.blockiesandroid.BlockiesIdenticon

import org.bitcoinj.core.Transaction
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.wallet.Wallet

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

import app.crescentcash.src.MainActivity
import app.crescentcash.src.R
import app.crescentcash.src.qr.QRHelper
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.wallet.SLPWalletHelper
import app.crescentcash.src.wallet.WalletHelper
import java.text.DecimalFormatSymbols
import java.util.*

class UIHelper {
    private val createWalletBtn: Button
    private val restoreWalletBtn: Button
    var restore_wallet: FrameLayout
    var new_wallet: FrameLayout
    var newuser: FrameLayout
    private val qrScan: Button
    private val openKeys: ImageButton
    private val balance: TextView

    /////REGISTER SCREEN
    private val registerUserBtn: Button
    var handle: EditText
    /////REGISTER SCREEN

    /////RECOVERY SCREEN
    private val verifyUserBtn: Button
    var recoverySeed: EditText
    var handle2: EditText
    /////RECOVERY SCREEN


    /////WALLET SETTINGS SCREEN
    var walletSettings: FrameLayout
    private val btnShowSeed: Button
    private val xpub: TextView
    private val closeSettingsBtn: Button
    /////WALLET SETTINGS SCREEN

    /////RECEIVE BITCOIN STUFF
    private val copyBtcAddr: ImageView
    private val btcAddress: TextView
    /////RECEIVE BITCOIN STUFF

    private val syncPct: TextView
    private val srlContent_AM: SwipeRefreshLayout
    private val srlHistory: SwipeRefreshLayout
    private val srlSLP: SwipeRefreshLayout
    private val myCashHandle: TextView
    private val tvRecipientAddress_AM: TextView
    val amountText: TextView
    val btnSend_AM: Button
    private val closeSendBtn: Button
    private val ivCopy_AM: ImageView
    private val sendTab: ImageButton
    private val receiveTab: ImageButton
    private val closeReceiveBtn: Button
    var sendWindow: FrameLayout
    var receiveWindow: FrameLayout
    var historyWindow: FrameLayout
    var slpBalancesWindow: FrameLayout
    private val historyTab: ImageButton
    private val closeHistoryBtn: Button
    private val setMaxCoins: Button
    private val txHistoryList: NonScrollListView
    private val slpList: NonScrollListView
    var txInfo: FrameLayout
    private val txInfoTV: TextView
    private val btnViewTx: Button
    private val btnCloseTx: Button
    var myEmoji: TextView
    var nightModeSwitch: Switch
    var sendUnit: TextView
    var addOpReturnSwitch: Switch
    private val donateBtn: TextView
    private val toggleAddr: ImageView
    private val viewSLPBtn: Button
    private val closeSLPBtn: Button
    var sendSLPWindow: FrameLayout
    val sendSLPBtn: Button
    private val closeSLPSendBtn: Button
    private val setMaxSLP: Button
    private val slpUnit: TextView
    var slpAmount: TextView
    var slpRecipientAddress: TextView
    private val slp_qrScan: Button
    private val fiatBalTxt: TextView
    var showFiatSwitch: Switch
    private val contactsBtn: ImageButton

    var emojis = intArrayOf(128123, 128018, 128021, 128008, 128014, 128004, 128022, 128016, 128042, 128024, 128000, 128007, 128063, 129415, 128019, 128039, 129414, 129417, 128034, 128013, 128031, 128025, 128012, 129419, 128029, 128030, 128375, 127803, 127794, 127796, 127797, 127809, 127808, 127815, 127817, 127819, 127820, 127822, 127826, 127827, 129373, 129381, 129365, 127805, 127798, 127812, 129472, 129370, 129408, 127850, 127874, 127853, 127968, 128663, 128690, 9973, 9992, 128641, 128640, 8986, 9728, 11088, 127752, 9730, 127880, 127872, 9917, 9824, 9829, 9830, 9827, 128083, 128081, 127913, 128276, 127925, 127908, 127911, 127928, 127930, 129345, 128269, 128367, 128161, 128214, 9993, 128230, 9999, 128188, 128203, 9986, 128273, 128274, 128296, 128295, 9878, 9775, 128681, 128099, 127838)

    private var currentAddrView = true

    val recipient: String
        get() = tvRecipientAddress_AM.text.toString().trim { it <= ' ' }

    //return flDownloadContent_LDP.getVisibility() == View.VISIBLE;
    val isDisplayingDownload: Boolean
        get() = MainActivity.INSTANCE.walletHelper.downloading

    val amount: String
        get() = amountText.text.toString()

    init {
        val mainActivity = MainActivity.INSTANCE

        createWalletBtn = mainActivity.findViewById(R.id.createWalletBtn)
        restoreWalletBtn = mainActivity.findViewById(R.id.restoreWalletBtn)
        restore_wallet = mainActivity.findViewById(R.id.restore_wallet)
        new_wallet = mainActivity.findViewById(R.id.new_wallet)
        newuser = mainActivity.findViewById(R.id.newuser)
        qrScan = mainActivity.findViewById(R.id.qrScan)
        openKeys = mainActivity.findViewById(R.id.openKeys)
        balance = mainActivity.findViewById(R.id.balance)
        registerUserBtn = mainActivity.findViewById(R.id.registerUserBtn)
        handle = mainActivity.findViewById(R.id.handle)
        verifyUserBtn = mainActivity.findViewById(R.id.verifyUserBtn)
        recoverySeed = mainActivity.findViewById(R.id.recoverySeed)
        handle2 = mainActivity.findViewById(R.id.handle2)
        walletSettings = mainActivity.findViewById(R.id.walletSettings)
        btnShowSeed = mainActivity.findViewById(R.id.btnShowSeed)
        xpub = mainActivity.findViewById(R.id.xpub)
        closeSettingsBtn = mainActivity.findViewById(R.id.closeSettingsBtn)
        copyBtcAddr = mainActivity.findViewById(R.id.copyBtcAddr)
        btcAddress = mainActivity.findViewById(R.id.btcAddress)
        srlContent_AM = mainActivity.findViewById(R.id.srlContent_AM)
        srlHistory = mainActivity.findViewById(R.id.srlHistory)
        myCashHandle = mainActivity.findViewById(R.id.myCashHandle)
        tvRecipientAddress_AM = mainActivity.findViewById(R.id.tvRecipientAddress_AM)
        amountText = mainActivity.findViewById(R.id.etAmount_AM)
        btnSend_AM = mainActivity.findViewById(R.id.btnSend_AM)
        closeSendBtn = mainActivity.findViewById(R.id.closeSendBtn)
        ivCopy_AM = mainActivity.findViewById(R.id.ivCopy_AM)
        sendTab = mainActivity.findViewById(R.id.sendTab)
        receiveTab = mainActivity.findViewById(R.id.receiveTab)
        closeReceiveBtn = mainActivity.findViewById(R.id.closeReceiveBtn)
        sendWindow = mainActivity.findViewById(R.id.sendWindow)
        receiveWindow = mainActivity.findViewById(R.id.receiveWindow)
        historyWindow = mainActivity.findViewById(R.id.historyWindow)
        historyTab = mainActivity.findViewById(R.id.historyTab)
        closeHistoryBtn = mainActivity.findViewById(R.id.closeHistoryBtn)
        setMaxCoins = mainActivity.findViewById(R.id.setMaxCoins)
        txHistoryList = mainActivity.findViewById(R.id.txHistoryList)
        txInfo = mainActivity.findViewById(R.id.txInfo)
        txInfoTV = mainActivity.findViewById(R.id.txInfoTV)
        btnCloseTx = mainActivity.findViewById(R.id.btnCloseTx)
        btnViewTx = mainActivity.findViewById(R.id.btnViewTx)
        syncPct = mainActivity.findViewById(R.id.syncPct)
        myEmoji = mainActivity.findViewById(R.id.myEmoji)
        nightModeSwitch = mainActivity.findViewById(R.id.nightModeSwitch)
        sendUnit = mainActivity.findViewById(R.id.sendUnit)
        addOpReturnSwitch = mainActivity.findViewById(R.id.addOpReturnSwitch)
        donateBtn = mainActivity.findViewById(R.id.donateBtn)
        toggleAddr = mainActivity.findViewById(R.id.toggleAddr)
        srlSLP = mainActivity.findViewById(R.id.srlSLP)
        slpBalancesWindow = mainActivity.findViewById(R.id.slpBalancesWindow)
        slpList = mainActivity.findViewById(R.id.slpList)
        viewSLPBtn = mainActivity.findViewById(R.id.viewSLPBtn)
        closeSLPBtn = mainActivity.findViewById(R.id.closeSLPBtn)
        sendSLPWindow = mainActivity.findViewById(R.id.sendSLPWindow)
        sendSLPBtn = mainActivity.findViewById(R.id.sendSLPBtn)
        closeSLPSendBtn = mainActivity.findViewById(R.id.closeSLPSendBtn)
        setMaxSLP = mainActivity.findViewById(R.id.setMaxSLP)
        slpUnit = mainActivity.findViewById(R.id.slpUnit)
        slpAmount = mainActivity.findViewById(R.id.slpAmount)
        slpRecipientAddress = mainActivity.findViewById(R.id.slpRecipientAddress)
        slp_qrScan = mainActivity.findViewById(R.id.slp_qrScan)
        fiatBalTxt = mainActivity.findViewById(R.id.fiatBalTxt)
        showFiatSwitch = mainActivity.findViewById(R.id.showFiatSwitch)
        contactsBtn = mainActivity.findViewById(R.id.contactsBtn)

        this.initListeners()
    }

    private fun initListeners() {
        this.restoreWalletBtn.setOnClickListener { displayRestore() }
        this.createWalletBtn.setOnClickListener { displayNewWallet() }
        this.srlContent_AM.setOnRefreshListener { this.refresh() }
        this.srlHistory.setOnRefreshListener { this.refresh() }
        this.srlSLP.setOnRefreshListener { this.refreshSLP() }

        this.btnSend_AM.setOnClickListener { MainActivity.INSTANCE.walletHelper.send() }
        this.ivCopy_AM.setOnClickListener {
            val clip = ClipData.newPlainText("My Cash Acct", myCashHandle.text.toString())
            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.copyBtcAddr.setOnClickListener {
            val clip = ClipData.newPlainText("My BCH address", btcAddress.text.toString())
            val clipboard = MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.primaryClip = clip
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show()
        }

        this.donateBtn.setOnClickListener { this.displayRecipientAddress("bitcoincash:qptnypuugy29lttleggl7l0vpls0vg295q9nsavw6g") }

        this.sendTab.setOnClickListener { displaySend() }
        this.receiveTab.setOnClickListener { displayReceive() }
        this.historyTab.setOnClickListener { displayHistory() }
        this.qrScan.setOnClickListener { clickScanQR() }
        this.openKeys.setOnClickListener { displayWalletKeys() }

        txHistoryList.setOnItemClickListener { parent, view, position, id ->
            val tx = MainActivity.INSTANCE.txList[position]
            displayTxWindow(tx)
        }

        this.nightModeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                nightModeEnabled = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("nightMode", nightModeEnabled).apply()
            }
        }

        this.addOpReturnSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                WalletHelper.addOpReturn = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("addOpReturn", WalletHelper.addOpReturn).apply()
            }
        }

        this.showFiatSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (MainActivity.INSTANCE.started) {
                showFiat = isChecked
                MainActivity.INSTANCE.prefs.edit().putBoolean("showFiat", showFiat).apply()
            }
        }

        this.toggleAddr.setOnClickListener { this.toggleAddress() }

        viewSLPBtn.setOnClickListener {
            MainActivity.INSTANCE.walletHelper.getSLPWallet().refreshBalance()

            setSLPList()
            slpBalancesWindow.visibility = View.VISIBLE
        }
        closeSLPBtn.setOnClickListener { this.slpBalancesWindow.visibility = View.GONE }

        slpList.setOnItemClickListener { parent, view, position, id ->
            val walletHelper = MainActivity.INSTANCE.walletHelper
            walletHelper.currentTokenPosition = position + 1
            walletHelper.currentTokenId = walletHelper.balances[walletHelper.currentTokenPosition].tokenId

            sendSLPWindow.visibility = View.VISIBLE
            slpUnit.text = walletHelper.balances[walletHelper.currentTokenPosition].ticker

        }

        setMaxSLP.setOnClickListener {
            val walletHelper = MainActivity.INSTANCE.walletHelper
            slpAmount.text = walletHelper.balances[walletHelper.currentTokenPosition].amount.toPlainString()
        }

        closeSLPSendBtn.setOnClickListener {
            sendSLPWindow.visibility = View.GONE
            this.slpBalancesWindow.visibility = View.VISIBLE
            this.slpRecipientAddress.text = null
            this.slpAmount.text = null
            MainActivity.INSTANCE.walletHelper.currentTokenId = ""
            MainActivity.INSTANCE.walletHelper.currentTokenPosition = 0
            setSLPList()
        }

        sendSLPBtn.setOnClickListener {
            if (!TextUtils.isEmpty(slpRecipientAddress.text) && !TextUtils.isEmpty(slpAmount.text)) {
                if (!slpRecipientAddress.text.toString().contains("#")) {
                    val tokenId = MainActivity.INSTANCE.walletHelper.currentTokenId
                    val tokenPos = MainActivity.INSTANCE.walletHelper.currentTokenPosition
                    val tokenInfo = MainActivity.INSTANCE.walletHelper.balances[tokenPos]
                    val amt = BigDecimal(java.lang.Double.parseDouble(slpAmount.text.toString())).setScale(tokenInfo.decimals!!, RoundingMode.HALF_UP)
                    println(amt.toPlainString())
                    MainActivity.INSTANCE.walletHelper.sendToken(tokenId, amt, slpRecipientAddress.text.toString())
                } else {
                    MainActivity.INSTANCE.runOnUiThread { this.showToastMessage("SLP CashAccts are not supported.") }
                }
            }
        }

        this.slp_qrScan.setOnClickListener { clickScanQR() }
    }

    private fun toggleAddress() {
        this.currentAddrView = !this.currentAddrView

        if (currentAddrView)
            this.displayAddress(MainActivity.INSTANCE.walletHelper.getSLPWallet().bchAddress)
        else
            this.displayAddress(MainActivity.INSTANCE.walletHelper.getSLPWallet().slpAddress)
    }

    private fun displayAddress(address: String) {
        btcAddress.text = address
        generateQR(address, R.id.btcQR)
    }

    private fun displayTxWindow(tx: Transaction) {
        val symbols = DecimalFormatSymbols(Locale.US)
        var decimalFormatter: DecimalFormat?
        var receivedValueStr = ""

        when {
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_BTC -> {
                decimalFormatter = DecimalFormat("#,###.########", symbols)
                receivedValueStr = MonetaryFormat.BTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_MBTC -> {
                decimalFormatter = DecimalFormat("#,###.#####", symbols)
                receivedValueStr = MonetaryFormat.MBTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_UBTC -> {
                decimalFormatter = DecimalFormat("#,###.##", symbols)
                receivedValueStr = MonetaryFormat.UBTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet)).toString()
                receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
                receivedValueStr = decimalFormatter.format(java.lang.Double.parseDouble(receivedValueStr))
            }
            MainActivity.INSTANCE.walletHelper.displayUnits == "sats" -> {
                decimalFormatter = DecimalFormat("#,###", symbols)
                val amt = java.lang.Double.parseDouble(tx.getValue(MainActivity.INSTANCE.walletHelper.wallet).toPlainString())
                val formatted = amt * 100000000

                val formattedStr = decimalFormatter.format(formatted)
                receivedValueStr = formattedStr
            }
        }

        receivedValueStr = receivedValueStr.replace(MainActivity.INSTANCE.walletHelper.displayUnits + " ", "")
        receivedValueStr = receivedValueStr.replace("-", "")
        val feeStr: String

        if (tx.fee != null) {
            decimalFormatter = DecimalFormat("#,###.########", symbols)
            var feeValueStr = MonetaryFormat.BTC.format(tx.fee).toString()
            feeValueStr = feeValueStr.replace("BCH ", "")
            val fee = java.lang.Float.parseFloat(feeValueStr)
            feeStr = decimalFormatter.format(fee.toDouble())
        } else {
            feeStr = "n/a"
        }

        val txConfirmations = tx.confidence

        val txConfirms = "" + txConfirmations.depthInBlocks
        val txDate = tx.updateTime.toString() + ""
        val txHash = tx.hashAsString
        txInfo.visibility = View.VISIBLE
        txInfoTV.text = Html.fromHtml("<b>" + MainActivity.INSTANCE.walletHelper.displayUnits + " Transferred:</b> " + receivedValueStr + "<br> <b>Fee:</b> " + feeStr + "<br> <b>Date:</b> " + txDate + "<br> <b>Confirmations:</b> " + txConfirms)
        btnCloseTx.setOnClickListener { v -> txInfo.visibility = View.GONE }
        btnViewTx.setOnClickListener { v ->
            val url = if (Constants.IS_PRODUCTION) "https://explorer.bitcoin.com/bch/tx/" else "https://explorer.bitcoin.com/tbch/tx/"
            val uri = Uri.parse(url + txHash) // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            MainActivity.INSTANCE.startActivity(intent)
        }
    }

    fun displayCashAccount(cashAccount: String) {
        myCashHandle.text = cashAccount

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }

    private fun displayCashAccount(wallet: Wallet, cashAccount: String?) {
        myCashHandle.text = cashAccount

        var bchAddress = ""
        var slpAddress = ""

        if (MainActivity.INSTANCE.walletHelper.walletKit != null) {
            bchAddress = MainActivity.INSTANCE.walletHelper.getSLPWallet().bchAddress
            slpAddress = MainActivity.INSTANCE.walletHelper.getSLPWallet().slpAddress

            if (currentAddrView) {
                btcAddress.text = bchAddress
                generateQR(bchAddress, R.id.btcQR)
            } else {
                btcAddress.text = slpAddress
                generateQR(slpAddress, R.id.btcQR)
            }
        } else {
            bchAddress = "Loading..."
            slpAddress = "Loading..."

            if (currentAddrView) {
                btcAddress.text = bchAddress
            } else {
                btcAddress.text = slpAddress
            }
        }

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }

    private fun displayEmoji(emoji: String) {
        myEmoji.text = emoji

        if (srlContent_AM.isRefreshing) srlContent_AM.isRefreshing = false
    }


    private fun displayNewWallet() {
        restore_wallet.visibility = View.GONE
        new_wallet.visibility = View.VISIBLE
        newuser.visibility = View.GONE

        registerUserBtn.setOnClickListener { MainActivity.INSTANCE.netHelper.prepareWalletForRegistration() }
    }

    private fun displayHistory() {
        setArrayAdapter(MainActivity.INSTANCE.walletHelper.wallet)

        historyWindow.visibility = View.VISIBLE

        closeHistoryBtn.setOnClickListener { hideHistoryView() }
    }

    private fun hideHistoryView() {
        historyWindow.visibility = View.GONE
    }

    private fun displayWalletKeys() {
        if(MainActivity.INSTANCE.walletHelper.walletKit != null) {
            val seed = MainActivity.INSTANCE.walletHelper.wallet.keyChainSeed
            val mnemonicCode = seed.mnemonicCode
            val recoverySeed = StringBuilder()

            assert(mnemonicCode != null)
            for (x in mnemonicCode!!.indices) {
                recoverySeed.append(mnemonicCode[x]).append(if (x == mnemonicCode.size - 1) "" else " ")
            }

            setWalletInfo(recoverySeed.toString())

            walletSettings.visibility = View.VISIBLE

            val dropdown = MainActivity.INSTANCE.findViewById<Spinner>(R.id.unitDropdown)
            val items = arrayOf(MonetaryFormat.CODE_BTC, MonetaryFormat.CODE_MBTC, MonetaryFormat.CODE_UBTC, "sats")
            val adapter = ArrayAdapter(MainActivity.INSTANCE, R.layout.spinner_item, items)
            dropdown.adapter = adapter

            when {
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_BTC -> dropdown.setSelection(0)
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_MBTC -> dropdown.setSelection(1)
                MainActivity.INSTANCE.walletHelper.displayUnits == MonetaryFormat.CODE_UBTC -> dropdown.setSelection(2)
                MainActivity.INSTANCE.walletHelper.displayUnits == "sats" -> dropdown.setSelection(3)
            }

            dropdown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, v: View, position: Int, id: Long) {

                    when (position) {
                        0 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_BTC
                        1 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_MBTC
                        2 -> MainActivity.INSTANCE.walletHelper.displayUnits = MonetaryFormat.CODE_UBTC
                        3 -> MainActivity.INSTANCE.walletHelper.displayUnits = "sats"
                    }

                    MainActivity.INSTANCE.prefs.edit().putString("displayUnit", MainActivity.INSTANCE.walletHelper.displayUnits).apply()
                    println(MainActivity.INSTANCE.walletHelper.displayUnits)
                    refresh()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }
            closeSettingsBtn.setOnClickListener { hideWalletKeys() }
        }
    }

    private fun hideWalletKeys() {
        walletSettings.visibility = View.GONE
        refresh()
    }

    private fun displayReceive() {
        val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")
        displayCashAccount(MainActivity.INSTANCE.walletHelper.wallet, cashAccount)
        sendWindow.visibility = View.GONE
        receiveWindow.visibility = View.VISIBLE
        closeReceiveBtn.setOnClickListener { hideReceiveView() }
    }

    private fun hideReceiveView() {
        receiveWindow.visibility = View.GONE
    }

    private fun displaySend() {
        sendWindow.visibility = View.VISIBLE
        receiveWindow.visibility = View.GONE
        sendUnit.text = MainActivity.INSTANCE.walletHelper.displayUnits
        closeSendBtn.setOnClickListener { hideSendView() }
        setMaxCoins.setOnClickListener { setMaxCoins() }
        contactsBtn.setOnClickListener { showContactSelectionScreen() }
    }

    private fun setMaxCoins() {
        var coins = balance.text.toString().replace(" " + MainActivity.INSTANCE.walletHelper.displayUnits, "")
        coins = coins.replace(",", "")
        amountText.text = coins
    }

    private fun hideSendView() {
        sendWindow.visibility = View.GONE
        amountText.text = null
        tvRecipientAddress_AM.text = null
    }

    private fun displayRestore() {
        MainActivity.isNewUser = false
        restore_wallet.visibility = View.VISIBLE
        new_wallet.visibility = View.GONE
        newuser.visibility = View.GONE

        verifyUserBtn.setOnClickListener { MainActivity.INSTANCE.netHelper.prepareWalletForVerification() }
    }

    @UiThread
    fun displayDownloadContent(status: Boolean) {
        MainActivity.INSTANCE.walletHelper.downloading = status

        if (!status) {
            syncPct.text = ""
        }

    }

    private fun setArrayAdapter(wallet: Wallet) {
        setListViewShit(wallet)

        if (srlHistory.isRefreshing) srlHistory.isRefreshing = false
    }

    private fun refreshSLP() {
        setSLPList()

        if (srlSLP.isRefreshing) srlSLP.isRefreshing = false
    }

    private fun setSLPList() {
        val walletHelper = MainActivity.INSTANCE.walletHelper
        walletHelper.getSLPWallet().refreshBalance()
        if (walletHelper.balances.isNotEmpty()) {
            val tokenList = ArrayList<Map<String, String>>()

            for (x in 1 until walletHelper.balances.size) {
                /*
                    x = 0 is BCH, as SLP SDK includes BCH in the balance list.
                     */
                val datum = HashMap<String, String>(2)
                val tokenName = walletHelper.balances[x].name
                val tokenTicker = walletHelper.balances[x].ticker
                val tokenHash = walletHelper.balances[x].tokenId
                val balance = walletHelper.balances[x].amount.toPlainString()
                val amt = BigDecimal(java.lang.Double.parseDouble(balance)).setScale(walletHelper.balances[x].decimals!!, RoundingMode.HALF_UP)

                datum["token"] = tokenName!!
                datum["tokenHash"] = tokenHash
                datum["tokenTicker"] = tokenTicker!!
                datum["balance"] = amt.toPlainString()
                tokenList.add(datum)
            }

            val itemsAdapter = object : SimpleAdapter(MainActivity.INSTANCE, tokenList, R.layout.list_view_activity, arrayOf("token", "balance", "tokenTicker"), intArrayOf(R.id.text1, R.id.text2, R.id.text3)) {
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                    // Get the Item from ListView
                    val view = super.getView(position, convertView, parent)

                    val slpBlockiesAddress = SLPWalletHelper.blockieAddressFromTokenId(tokenList[position]["tokenHash"] ?: error(""))

                    val slpIcon = view.findViewById<BlockiesIdenticon>(R.id.slpImage)
                    slpIcon.setAddress(slpBlockiesAddress)
                    slpIcon.setCornerRadius(128f)
                    // Initialize a TextView for ListView each Item
                    val text1 = view.findViewById<TextView>(R.id.text1)
                    val text2 = view.findViewById<TextView>(R.id.text2)
                    val text3 = view.findViewById<TextView>(R.id.text3)

                    // Set the text color of TextView (ListView Item)

                    if (nightModeEnabled) {
                        text1.setTextColor(Color.WHITE)
                        text2.setTextColor(Color.GRAY)
                        text3.setTextColor(Color.WHITE)
                    } else {
                        text1.setTextColor(Color.BLACK)
                        text3.setTextColor(Color.BLACK)
                    }

                    text2.ellipsize = TextUtils.TruncateAt.END
                    text2.maxLines = 1
                    text2.setSingleLine(true)
                    // Generate ListView Item using TextView
                    return view
                }
            }
            MainActivity.INSTANCE.runOnUiThread {
                slpList.adapter = itemsAdapter
                slpList.refreshDrawableState()
            }
        }
    }

    private fun setListViewShit(wallet: Wallet?) {
        if (wallet != null) {
            val symbols = DecimalFormatSymbols(Locale.US)
            val txListFromWallet = wallet.getRecentTransactions(100, false)

            if (txListFromWallet != null && txListFromWallet.size != 0) {
                val txListFormatted = ArrayList<Map<String, String>>()
                MainActivity.INSTANCE.txList = ArrayList()

                val sizeToUse: Int
                if (txListFromWallet.size >= 100)
                    sizeToUse = 100
                else
                    sizeToUse = txListFromWallet.size

                for (x in 0 until sizeToUse) {
                    val tx = txListFromWallet[x]
                    val confirmations = tx.confidence.depthInBlocks
                    val value = tx.getValue(wallet)
                    val datum = HashMap<String, String>(2)

                    if (value.isPositive) {
                        var receivedValueStr = ""

                        val unit = MainActivity.INSTANCE.walletHelper.displayUnits
                        var formatted = java.lang.Double.parseDouble(value.toPlainString())

                        when (unit) {
                            MonetaryFormat.CODE_BTC -> {
                                val formatter = DecimalFormat("#,###.########", symbols)
                                val formattedStr = formatter.format(formatted)
                                receivedValueStr = "$formattedStr $unit"
                            }
                            MonetaryFormat.CODE_MBTC -> {
                                formatted *= 1000
                                val formatter = DecimalFormat("#,###.#####", symbols)
                                val formattedStr = formatter.format(formatted)
                                receivedValueStr = "$formattedStr $unit"
                            }
                            MonetaryFormat.CODE_UBTC -> {
                                formatted *= 1000000
                                val formatter = DecimalFormat("#,###.##", symbols)
                                val formattedStr = formatter.format(formatted)
                                receivedValueStr = "$formattedStr $unit"
                            }
                            "sats" -> {
                                formatted *= 100000000
                                val formatter = DecimalFormat("#,###", symbols)

                                val formattedStr = formatter.format(formatted)
                                receivedValueStr = "$formattedStr sats"
                            }
                        }

                        val entry = String.format("▶ %5s", receivedValueStr)
                        datum["amount"] = entry
                        txListFormatted.add(datum)
                        MainActivity.INSTANCE.txList.add(tx)
                    }

                    if (value.isNegative) {
                        var sentValueStr = value.toPlainString().replace("-", "")

                        val unit = MainActivity.INSTANCE.walletHelper.displayUnits
                        var formatted = java.lang.Double.parseDouble(sentValueStr)

                        when (unit) {
                            MonetaryFormat.CODE_BTC -> {
                                val formatter = DecimalFormat("#,###.########", symbols)
                                val formattedStr = formatter.format(formatted)
                                sentValueStr = "$formattedStr $unit"
                            }
                            MonetaryFormat.CODE_MBTC -> {
                                formatted *= 1000
                                val formatter = DecimalFormat("#,###.#####", symbols)
                                val formattedStr = formatter.format(formatted)
                                sentValueStr = "$formattedStr $unit"
                            }
                            MonetaryFormat.CODE_UBTC -> {
                                formatted *= 1000000
                                val formatter = DecimalFormat("#,###.##", symbols)
                                val formattedStr = formatter.format(formatted)
                                sentValueStr = "$formattedStr $unit"
                            }
                            "sats" -> {
                                formatted *= 100000000
                                val formatter = DecimalFormat("#,###", symbols)

                                val formattedStr = formatter.format(formatted)
                                sentValueStr = "$formattedStr sats"
                            }
                        }

                        val entry = String.format("◀ %5s", sentValueStr)
                        datum["amount"] = entry
                        txListFormatted.add(datum)
                        MainActivity.INSTANCE.txList.add(tx)
                    }

                    if (confirmations == 0) {
                        datum["confirmations"] = "0/unconfirmed"
                    } else if (confirmations < 6) {
                        datum["confirmations"] = "$confirmations/6 confirmations"
                    } else {
                        datum["confirmations"] = "6+ confirmations"
                    }
                }

                val itemsAdapter = object : SimpleAdapter(MainActivity.INSTANCE, txListFormatted, android.R.layout.simple_list_item_2, arrayOf("amount", "confirmations"), intArrayOf(android.R.id.text1, android.R.id.text2)) {
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                        // Get the Item from ListView
                        val view = super.getView(position, convertView, parent)

                        // Initialize a TextView for ListView each Item
                        val text1 = view.findViewById<View>(android.R.id.text1) as TextView
                        val text2 = view.findViewById<View>(android.R.id.text2) as TextView

                        // Set the text color of TextView (ListView Item)

                        if (nightModeEnabled) {
                            text1.setTextColor(Color.WHITE)
                            text2.setTextColor(Color.GRAY)
                        } else {
                            text1.setTextColor(Color.BLACK)
                        }

                        text2.ellipsize = TextUtils.TruncateAt.END
                        text2.maxLines = 1
                        text2.setSingleLine(true)
                        // Generate ListView Item using TextView
                        return view
                    }
                }
                MainActivity.INSTANCE.runOnUiThread { txHistoryList.adapter = itemsAdapter }
            }
        }
    }

    private fun clickScanQR() {
        val qrHelper = QRHelper()
        qrHelper.startQRScan(MainActivity.INSTANCE)
    }

    fun displayRecipientAddress(recipientAddress: String?) {
        if (recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.hint = MainActivity.INSTANCE.resources.getString(R.string.receiver)
            } else {
                tvRecipientAddress_AM.text = recipientAddress
            }
        } else {
            tvRecipientAddress_AM.text = null
            tvRecipientAddress_AM.hint = MainActivity.INSTANCE.resources.getString(R.string.receiver)
        }
    }

    fun showToastMessage(message: String) {
        Toast.makeText(MainActivity.INSTANCE, message, Toast.LENGTH_SHORT).show()
    }

    fun showContactSelectionScreen()
    {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        pickContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        MainActivity.INSTANCE.startActivityForResult(pickContact, 14)
    }

    private fun setWalletInfo(recoverySeed: String) {
        this.xpub.text = MainActivity.INSTANCE.walletHelper.getXPUB()

        this.btnShowSeed.setOnClickListener { v ->
            val builder = AlertDialog.Builder(MainActivity.INSTANCE, if (UIHelper.nightModeEnabled) R.style.AlertDialogDark else R.style.AlertDialogLight)
            builder.setTitle("Your recovery seed:")
            builder.setMessage(recoverySeed)
            builder.setCancelable(true)
            builder.setPositiveButton("Hide") { dialog, which -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.show()
            val msgTxt = alertDialog.findViewById<View>(android.R.id.message) as TextView
            msgTxt.movementMethod = LinkMovementMethod.getInstance()

        }
    }

    @UiThread
    fun displayProgress(percent: Int) {
        //if(pbProgress_LDP.isIndeterminate()) pbProgress_LDP.setIndeterminate(false);
        //pbProgress_LDP.setProgress(percent);
    }

    @UiThread
    fun displayPercentage(percent: Int) {
        if (MainActivity.INSTANCE.walletHelper.downloading)
            syncPct.text = "Syncing... $percent%"
        //tvPercentage_LDP.setText(percent + " %");
    }

    @UiThread
    fun displayMyBalance(myBalance: String) {
        MainActivity.INSTANCE.runOnUiThread {
            val symbols = DecimalFormatSymbols(Locale.US)
            var balanceStr = myBalance
            balanceStr = balanceStr.replace(" BCH", "")
            var formatted = java.lang.Double.parseDouble(balanceStr)

            val unit = MainActivity.INSTANCE.walletHelper.displayUnits

            when (unit) {
                MonetaryFormat.CODE_BTC -> {
                    val formatter = DecimalFormat("#,###.########", symbols)
                    val formattedStr = formatter.format(formatted)
                    balance.text = "$formattedStr $unit"
                }
                MonetaryFormat.CODE_MBTC -> {
                    formatted *= 1000
                    val formatter = DecimalFormat("#,###.#####", symbols)
                    val formattedStr = formatter.format(formatted)
                    balance.text = "$formattedStr $unit"
                }
                MonetaryFormat.CODE_UBTC -> {
                    formatted *= 1000000
                    val formatter = DecimalFormat("#,###.##", symbols)
                    val formattedStr = formatter.format(formatted)
                    balance.text = "$formattedStr $unit"
                }
                "sats" -> {
                    formatted *= 100000000
                    val formatter = DecimalFormat("#,###", symbols)

                    val formattedStr = formatter.format(formatted)
                    balance.text = "$formattedStr $unit"
                }
            }
        }
        //balanceStr = "\u20BF" + balanceStr;
        //balance.setText(balanceStr);
    }

    private fun generateQR(textToConvert: String, viewID: Int) {
        try {
            val encoder = BarcodeEncoder()

            val qrCode = encoder.encodeBitmap(textToConvert, BarcodeFormat.QR_CODE, 300, 300)

            if (this.currentAddrView) {
                val coinLogo = drawableToBitmap(MainActivity.INSTANCE.resources.getDrawable(R.drawable.logo_bch))
                val merge = overlayBitmapToCenter(qrCode, coinLogo!!)
                (MainActivity.INSTANCE.findViewById<View>(viewID) as ImageView).setImageBitmap(merge)
            } else {
                val coinLogo = drawableToBitmap(MainActivity.INSTANCE.resources.getDrawable(R.drawable.logo_slp))

                val newCoinLogo = getResizedBitmap(coinLogo!!, 55, 55)
                val merge = overlayBitmapToCenter(qrCode, newCoinLogo)
                (MainActivity.INSTANCE.findViewById<View>(viewID) as ImageView).setImageBitmap(merge)
            }
        } catch (e: WriterException) {
            e.printStackTrace()
        }


    }

    /*
    I'm absolutely terrible with Bitmap and image generation shit. Always have been.

    Shout-out to StackOverflow for some of this.
     */
    private fun overlayBitmapToCenter(bitmap1: Bitmap, bitmap2: Bitmap): Bitmap {
        val bitmap1Width = bitmap1.width
        val bitmap1Height = bitmap1.height
        val bitmap2Width = bitmap2.width
        val bitmap2Height = bitmap2.height

        val marginLeft = (bitmap1Width * 0.5 - bitmap2Width * 0.5).toFloat()
        val marginTop = (bitmap1Height * 0.5 - bitmap2Height * 0.5).toFloat()

        val overlayBitmap = Bitmap.createBitmap(bitmap1Width, bitmap1Height, bitmap1.config)
        val canvas = Canvas(overlayBitmap)
        canvas.drawBitmap(bitmap1, Matrix(), null)
        canvas.drawBitmap(bitmap2, marginLeft, marginTop, null)
        return overlayBitmap
    }

    private fun drawableToBitmap(drawable: Drawable): Bitmap? {
        val bitmap: Bitmap? = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
        } else {
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun getResizedBitmap(bm: Bitmap, newWidth: Int, newHeight: Int): Bitmap {
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP

        return Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false)
    }

    fun clearAmount() {
        amountText.text = null
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }

        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun refresh() {
        MainActivity.INSTANCE.walletHelper.serializedXpub = MainActivity.INSTANCE.walletHelper.wallet.watchingKey.serializePubB58(MainActivity.INSTANCE.walletHelper.parameters)

        if (showFiat) {
            object : Thread() {
                override fun run() {
                    val price = MainActivity.INSTANCE.netHelper.price
                    val coinBal = java.lang.Double.parseDouble(MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).toPlainString())
                    val coinFiat = coinBal * price
                    val symbols = DecimalFormatSymbols(Locale.US)
                    val df = DecimalFormat("#,###.##", symbols)

                    val usdBal = "$" + df.format(coinFiat)

                    MainActivity.INSTANCE.runOnUiThread { fiatBalTxt.text = usdBal }
                }
            }.start()
        } else {
            fiatBalTxt.text = ""
        }

        displayMyBalance(MainActivity.INSTANCE.walletHelper.getBalance(MainActivity.INSTANCE.walletHelper.wallet).toFriendlyString())

        val seed = MainActivity.INSTANCE.walletHelper.wallet.keyChainSeed
        val mnemonicCode = seed.mnemonicCode
        val recoverySeed = StringBuilder()

        assert(mnemonicCode != null)
        for (x in mnemonicCode!!.indices) {
            recoverySeed.append(mnemonicCode[x]).append(" ")
        }

        val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")
        var cashEmoji = MainActivity.INSTANCE.prefs.getString("cashEmoji", "")

        if (cashAccount!!.contains("#???")) {
            cashEmoji = "?"
            val cashAcctPlain = cashAccount.replace("#???", "")
            MainActivity.INSTANCE.netHelper.initialAccountIdentityCheck(cashAcctPlain)
        }

        if(MainActivity.INSTANCE.walletHelper.slpWallet != null){
            this.displayCashAccount(MainActivity.INSTANCE.walletHelper.wallet, cashAccount)
            MainActivity.INSTANCE.walletHelper.getSLPWallet().refreshBalance()

        }

        this.displayEmoji(cashEmoji)
        this.setWalletInfo(recoverySeed.toString())

        if (historyWindow.visibility == View.VISIBLE) {
            setArrayAdapter(MainActivity.INSTANCE.walletHelper.wallet)
        }
    }

    companion object {

        var nightModeEnabled = false
        var showFiat = true
    }
}
