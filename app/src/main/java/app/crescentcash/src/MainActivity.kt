package app.crescentcash.src

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import android.view.View

import com.google.common.util.concurrent.ListenableFuture
import com.google.zxing.integration.android.IntentIntegrator

import org.bitcoinj.core.Transaction
import org.bitcoinj.protocols.payments.PaymentProtocolException
import org.bitcoinj.protocols.payments.PaymentSession
import org.bitcoinj.utils.MonetaryFormat

import java.text.DecimalFormat
import java.util.ArrayList
import java.util.concurrent.ExecutionException

import app.crescentcash.src.hash.HashHelper
import app.crescentcash.src.net.NetHelper
import app.crescentcash.src.ui.UIHelper
import app.crescentcash.src.utils.PermissionHelper
import app.crescentcash.src.wallet.SLPWalletHelper
import app.crescentcash.src.wallet.WalletHelper
import android.widget.Toast
import android.provider.ContactsContract



class MainActivity : AppCompatActivity() {
    lateinit var uiHelper: UIHelper
    lateinit var walletHelper: WalletHelper
    lateinit var netHelper: NetHelper

    lateinit var txList: ArrayList<Transaction>
    lateinit var prefs: SharedPreferences
    var started = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        started = false
        INSTANCE = this
        this.prefs = getSharedPreferences("app.crescentcash.src", Context.MODE_PRIVATE)
        UIHelper.nightModeEnabled = prefs.getBoolean("nightMode", false)
        UIHelper.showFiat = prefs.getBoolean("showFiat", true)
        WalletHelper.addOpReturn = prefs.getBoolean("addOpReturn", true)
        setTheme(if (UIHelper.nightModeEnabled) R.style.CrescentCashDark else R.style.CrescentCashLight)
        SLPWalletHelper.setAPIKey()

        this.setContentView(R.layout.activity_main)
        this.uiHelper = UIHelper()
        this.netHelper = NetHelper()
        this.walletHelper = WalletHelper()
        this.uiHelper.nightModeSwitch.isChecked = UIHelper.nightModeEnabled
        this.uiHelper.addOpReturnSwitch.isChecked = WalletHelper.addOpReturn
        this.uiHelper.showFiatSwitch.isChecked = UIHelper.showFiat

        isNewUser = this.prefs.getBoolean("isNewUser", true)

        if (isNewUser) {
            this.uiHelper.newuser.visibility = View.VISIBLE
        } else {
            val hasEmoji = this.prefs.getString("cashEmoji", "")

            if (hasEmoji != null) {
                if (hasEmoji == "?" || hasEmoji == "") {
                    object : Thread() {
                        override fun run() {
                            val cashAcctTx = MainActivity.INSTANCE.prefs.getString("cashAcctTx", "null") as String
                            walletHelper.registeredTxHash = cashAcctTx

                            if (cashAcctTx != "null") {
                                try {
                                    walletHelper.registeredBlockHash = netHelper.getTransactionsBlockHash(walletHelper.registeredTxHash)

                                    if (walletHelper.registeredBlock != "???") {
                                        try {
                                            val emoji = HashHelper().getCashAccountEmoji(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                            prefs.edit().putString("cashEmoji", emoji).apply()
                                            uiHelper.myEmoji.text = emoji
                                        } catch (e: Exception) {
                                            uiHelper.myEmoji.text = "?"
                                        }

                                    } else {
                                        uiHelper.myEmoji.text = "?"
                                    }
                                } catch (e: NullPointerException) {
                                    e.printStackTrace()
                                    uiHelper.myEmoji.text = "?"
                                }

                            } else {
                                try {
                                    val cashAcctName = MainActivity.INSTANCE.prefs.getString("cashAccount", "") as String
                                    val cashAcctEmoji = netHelper.getCashAccountEmoji(cashAcctName)
                                    prefs.edit().putString("cashEmoji", cashAcctEmoji).apply()
                                    uiHelper.myEmoji.text = cashAcctEmoji
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                    runOnUiThread { uiHelper.showToastMessage("Error getting emoji") }
                                }

                            }
                        }
                    }.start()
                } else {
                    uiHelper.myEmoji.text = hasEmoji
                }
            }

            val hasSLPWallet = this.prefs.getBoolean("hasSLPWallet", false)

            if (hasSLPWallet)
                this.walletHelper.setupSLPWallet(null, true)

            this.walletHelper.setupWalletKit(null, "", false)

            this.uiHelper.displayDownloadContent(true)

            val cashAcct = MainActivity.INSTANCE.prefs.getString("cashAccount", "")!!

            if (cashAcct != "" && cashAcct.contains("#???")) {
                val plainName = cashAcct.replace("#???", "")
                this.walletHelper.registeredBlock = "???"
                this.netHelper.initialAccountIdentityCheck(plainName)

                this.walletHelper.timer = object : CountDownTimer(150000, 20) {

                    override fun onTick(millisUntilFinished: Long) {

                    }

                    override fun onFinish() {
                        try {
                            netHelper.checkForAccountIdentity(plainName)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }.start()
            }

        }

        val permissionHelper = PermissionHelper()
        permissionHelper.askForPermissions(this, this)

        started = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode != 14) {
            val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (scanResult != null) {
                if (this.uiHelper.sendSLPWindow.visibility != View.VISIBLE) {
                    val address = scanResult.contents
                    if (address != null) {
                        if (address.contains(this.walletHelper.parameters.cashAddrPrefix)) {
                            //url variable for BitPay invoice IDs
                            if (address.contains("?r=")) {
                                val addressFixed = address.replace(this.walletHelper.parameters.cashAddrPrefix + ":", "")
                                val addressFixed2 = addressFixed.replace("?r=", "")
                                this.uiHelper.displayRecipientAddress(addressFixed2)

                                if (addressFixed2.startsWith("https")) {
                                    object : Thread() {
                                        override fun run() {
                                            try {
                                                val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(addressFixed2)

                                                val session = future.get()

                                                val amountWanted = session.value
                                                val df = DecimalFormat("#,###.########")
                                                runOnUiThread {

                                                    when (MainActivity.INSTANCE.walletHelper.displayUnits) {
                                                        MonetaryFormat.CODE_BTC -> {
                                                            uiHelper.amountText.text = df.format(java.lang.Double.parseDouble(amountWanted.toPlainString())).replace(",", "")
                                                            println("Amount to pay: " + df.format(java.lang.Double.parseDouble(amountWanted.toPlainString())))
                                                        }
                                                        MonetaryFormat.CODE_MBTC -> {
                                                            var amt = java.lang.Double.parseDouble(amountWanted.toPlainString())
                                                            amt *= 1000.0
                                                            uiHelper.amountText.text = df.format(amt).replace(",", "")
                                                            println("Amount to pay: " + df.format(amt))
                                                        }
                                                        MonetaryFormat.CODE_UBTC -> {
                                                            var amt = java.lang.Double.parseDouble(amountWanted.toPlainString())
                                                            amt *= 1000000.0
                                                            uiHelper.amountText.text = df.format(amt).replace(",", "")
                                                            println("Amount to pay: " + df.format(amt))
                                                        }
                                                        "sats" -> {
                                                            var amt = java.lang.Double.parseDouble(amountWanted.toPlainString())
                                                            amt *= 100000000.0
                                                            uiHelper.amountText.text = df.format(amt).replace(",", "")
                                                            println("Amount to pay: " + df.format(amt))
                                                        }
                                                    }
                                                }
                                            } catch (e: InterruptedException) {
                                                e.printStackTrace()
                                            } catch (e: ExecutionException) {
                                                e.printStackTrace()
                                            } catch (e: PaymentProtocolException) {
                                                e.printStackTrace()
                                            }

                                        }
                                    }.start()
                                }
                            } else {
                                if (address.contains("?")) {
                                    netHelper.handleURI(address)
                                } else {
                                    this.uiHelper.displayRecipientAddress(address)
                                }
                            }
                        } else {
                            if (address.contains("?")) {
                                netHelper.handleURI(address)
                            } else {
                                this.uiHelper.displayRecipientAddress(address)
                            }
                        }
                    }
                } else {
                    val address = scanResult.contents
                    if (address != null) {
                        this.uiHelper.slpRecipientAddress.text = address
                    }
                }
            }
        }
        else
        {
            if(data != null) {
                if (data.data != null) {
                    val contactData = data.data
                    val c = contentResolver.query(contactData, null, null, null, null)
                    if (c!!.moveToFirst()) {
                        val phoneIndex = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                        val num = c.getString(phoneIndex)
                        uiHelper.displayRecipientAddress(num)
                        c.close()
                    }
                } else {
                    uiHelper.showToastMessage("No contact selected.")
                }
            } else {
                uiHelper.showToastMessage("No contact selected.")
            }
        }
    }

    override fun onBackPressed() {

        when {
            this.uiHelper.new_wallet.visibility == View.VISIBLE -> {
                this.uiHelper.new_wallet.visibility = View.GONE
                this.uiHelper.newuser.visibility = View.VISIBLE
            }
            this.uiHelper.restore_wallet.visibility == View.VISIBLE -> {
                this.uiHelper.restore_wallet.visibility = View.GONE
                this.uiHelper.newuser.visibility = View.VISIBLE
            }
            this.uiHelper.sendWindow.visibility == View.VISIBLE -> {
                this.uiHelper.sendWindow.visibility = View.GONE
                this.uiHelper.clearAmount()
                this.uiHelper.displayRecipientAddress(null)
            }
            this.uiHelper.sendSLPWindow.visibility == View.VISIBLE -> {
                this.uiHelper.sendSLPWindow.visibility = View.GONE
                this.uiHelper.slpAmount.text = null
                this.uiHelper.slpRecipientAddress.text = null
            }
            this.uiHelper.slpBalancesWindow.visibility == View.VISIBLE -> this.uiHelper.slpBalancesWindow.visibility = View.GONE
            this.uiHelper.receiveWindow.visibility == View.VISIBLE -> this.uiHelper.receiveWindow.visibility = View.GONE
            this.uiHelper.historyWindow.visibility == View.VISIBLE -> this.uiHelper.historyWindow.visibility = View.GONE
            this.uiHelper.walletSettings.visibility == View.VISIBLE -> this.uiHelper.walletSettings.visibility = View.GONE
            this.uiHelper.txInfo.visibility == View.VISIBLE -> this.uiHelper.txInfo.visibility = View.GONE
            else -> {
                val intent = Intent(Intent.ACTION_MAIN)
                intent.addCategory(Intent.CATEGORY_HOME)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    companion object {
        lateinit var INSTANCE: MainActivity
        var isNewUser = true
    }
}
