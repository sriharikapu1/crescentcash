package app.crescentcash.src.wallet

import android.content.Context
import android.os.CountDownTimer
import android.os.Looper
import android.os.Vibrator
import android.text.TextUtils
import android.view.View
import android.os.Handler
import androidx.lifecycle.Observer

import com.bitcoin.slpwallet.Network
import com.bitcoin.slpwallet.SLPWallet
import com.bitcoin.slpwallet.presentation.BalanceInfo
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture

import org.bitcoinj.core.Address
import org.bitcoinj.core.AddressFactory
import org.bitcoinj.core.AddressFormatException
import org.bitcoinj.core.Coin
import org.bitcoinj.core.InsufficientMoneyException
import org.bitcoinj.core.NetworkParameters
import org.bitcoinj.core.Transaction
import org.bitcoinj.core.listeners.DownloadProgressTracker
import org.bitcoinj.kits.WalletAppKit
import org.bitcoinj.params.MainNetParams
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.protocols.payments.PaymentProtocol
import org.bitcoinj.protocols.payments.PaymentProtocolException
import org.bitcoinj.protocols.payments.PaymentSession
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.utils.BriefLogFormatter
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.utils.Threading
import org.bitcoinj.wallet.DeterministicSeed
import org.bitcoinj.wallet.SendRequest
import org.bitcoinj.wallet.Wallet

import java.io.File
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.concurrent.ExecutionException

import app.crescentcash.src.MainActivity
import app.crescentcash.src.net.NetHelper
import app.crescentcash.src.ui.UIHelper
import app.crescentcash.src.utils.Constants
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.concurrent.Executor

class WalletHelper {
    private val walletDir: File = File(MainActivity.INSTANCE.applicationInfo.dataDir)

    var parameters: NetworkParameters = if (Constants.IS_PRODUCTION) MainNetParams.get() else TestNet3Params.get()
    var walletKit: WalletAppKit? = null
    var slpWallet: SLPWallet? = null
    lateinit var serializedXpub: String
    private val uiHelper: UIHelper
    private val netHelper: NetHelper
    var registeredTxHash = "null"
    var registeredBlock = "null"
    var registeredBlockHash = "null"
    lateinit var timer: CountDownTimer
    var downloading: Boolean = false
    var displayUnits: String
    lateinit var balances: List<BalanceInfo>

    lateinit var currentTokenId: String
    var currentTokenPosition: Int = 0

    val wallet: Wallet
        get() = walletKit!!.wallet()

    private var formattedStr: String? = null

    init {
        BriefLogFormatter.init()

        uiHelper = MainActivity.INSTANCE.uiHelper
        netHelper = MainActivity.INSTANCE.netHelper

        displayUnits = MainActivity.INSTANCE.prefs.getString("displayUnit", MonetaryFormat.CODE_BTC) as String
    }

    fun setupWalletKit(seed: DeterministicSeed?, cashAcctName: String, verifyingRestore: Boolean) {
        setBitcoinSDKThread()
        walletKit = object : WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
            override fun onSetupCompleted() {
                wallet().allowSpendingUnconfirmedTransactions()
                setupWalletListeners(wallet())
                serializedXpub = wallet.watchingKey.serializePubB58(parameters)

                if (MainActivity.isNewUser) {
                    val address = getSLPWallet().bchAddress

                    println("Registering...")
                    MainActivity.INSTANCE.prefs.edit().putBoolean("isNewUser", false).apply()
                    if(Constants.IS_PRODUCTION) netHelper.registerCashAccount(cashAcctName, address)
                } else {
                    if(Constants.IS_PRODUCTION) {
                        if (verifyingRestore) {
                            try {
                                val cashAcctAddress = org.bitcoinj.net.NetHelper().getCashAccountAddress(parameters, cashAcctName)
                                val cashAcctEmoji = netHelper.getCashAccountEmoji(cashAcctName)
                                println(cashAcctAddress)
                                var accountAddress: Address? = null

                                if (Address.isValidCashAddr(parameters, cashAcctAddress)) {
                                    accountAddress = AddressFactory.create().getAddress(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddress)
                                } else if (Address.isValidLegacyAddress(parameters, cashAcctAddress)) {
                                    accountAddress = Address.fromBase58(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddress)
                                } else {
                                    uiHelper.showToastMessage("No address found!")
                                }

                                if (accountAddress != null) {
                                    val isAddressMine = isAddressMine(accountAddress.toString())

                                    if (isAddressMine) {
                                        val pref = MainActivity.INSTANCE.prefs.edit()
                                        pref.putString("cashAccount", cashAcctName)
                                        pref.putString("cashEmoji", cashAcctEmoji)
                                        pref.putBoolean("isNewUser", false)
                                        pref.apply()

                                        MainActivity.INSTANCE.runOnUiThread {
                                            uiHelper.restore_wallet.visibility = View.GONE
                                            uiHelper.displayDownloadContent(true)
                                            uiHelper.myEmoji.text = cashAcctEmoji
                                            uiHelper.showToastMessage("Verified!")
                                            uiHelper.refresh()
                                        }
                                    } else {
                                        walletKit!!.stopAsync()
                                        walletKit = null
                                        MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("This is not your Cash Account!") }
                                    }
                                }
                            } catch (e: NullPointerException) {
                                if (walletKit != null) {
                                    walletKit!!.stopAsync()
                                    walletKit = null
                                }
                                MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Cash Account not found.") }
                            }

                        } else {
                            val hasSLPWallet = MainActivity.INSTANCE.prefs.getBoolean("hasSLPWallet", false)

                            if (!hasSLPWallet) {
                                val mainHandler = Handler(Looper.getMainLooper())

                                val myRunnable = { setupSLPWallet(null, false) }
                                mainHandler.post(myRunnable)
                            }

                            if (seed == null)
                                uiHelper.refresh()
                        }
                    } else {
                        if(verifyingRestore)
                        {
                            val pref = MainActivity.INSTANCE.prefs.edit()
                            pref.putString("cashAccount", cashAcctName)
                            pref.putString("cashEmoji", "")
                            pref.putBoolean("isNewUser", false)
                            pref.apply()

                            MainActivity.INSTANCE.runOnUiThread {
                                uiHelper.restore_wallet.visibility = View.GONE
                                uiHelper.displayDownloadContent(true)
                                uiHelper.myEmoji.text = ""
                                uiHelper.showToastMessage("Verified!")
                                uiHelper.refresh()
                            }
                        }
                        else
                        {
                            val hasSLPWallet = MainActivity.INSTANCE.prefs.getBoolean("hasSLPWallet", false)

                            if(!hasSLPWallet)
                            {
                                val mainHandler = Handler(Looper.getMainLooper())

                                val myRunnable = { setupSLPWallet(null, false) }
                                mainHandler.post(myRunnable)
                            }
                        }
                    }
                }
            }
        }

        walletKit!!.setDownloadListener(object : DownloadProgressTracker() {
            override fun progress(pct: Double, blocksSoFar: Int, date: Date) {
                super.progress(pct, blocksSoFar, date)
                val percentage = pct.toInt()
                MainActivity.INSTANCE.runOnUiThread {
                    uiHelper.displayPercentage(percentage)
                    uiHelper.displayProgress(percentage)
                }

            }

            override fun doneDownload() {
                super.doneDownload()
                MainActivity.INSTANCE.runOnUiThread {
                    uiHelper.displayDownloadContent(false)
                    uiHelper.refresh()
                }
            }
        })

        if (seed != null)
            walletKit!!.restoreWalletFromSeed(seed)

        walletKit!!.setBlockingStartup(false)
        walletKit!!.startAsync()
    }

    private fun isAddressMine(address: String): Boolean {
        val addressObj = AddressFactory.create().getAddress(parameters, address)

        return wallet.isPubKeyHashMine(addressObj.hash160)
    }

    private fun setBitcoinSDKThread() {
        val handler = Handler()
        Threading.USER_THREAD = Executor { handler.post(it) }
    }

    fun getBalance(wallet: Wallet): Coin {
        return wallet.getBalance(Wallet.BalanceType.ESTIMATED)
    }

    fun getSLPWallet(): SLPWallet {
        return slpWallet!!
    }

    fun send() {
        if (!uiHelper.isDisplayingDownload) {
            val symbols = DecimalFormatSymbols(Locale.US)
            uiHelper.btnSend_AM.isEnabled = false
            val amount = uiHelper.amount
            val amtDblToFrmt: Double

            if (!TextUtils.isEmpty(amount)) {
                amtDblToFrmt = java.lang.Double.parseDouble(amount)
            } else {
                amtDblToFrmt = 0.0
            }

            val df = DecimalFormat("#.########", symbols)
            df.roundingMode = RoundingMode.CEILING

            val amtDblFrmt = df.format(amtDblToFrmt)

            var amtToSend = java.lang.Double.parseDouble(amtDblFrmt)
            val formatter = DecimalFormat("#.########", symbols)

            if (displayUnits == MonetaryFormat.CODE_BTC) {
                println("No formatting needed")
                formattedStr = formatter.format(amtToSend)
            }

            if (displayUnits == MonetaryFormat.CODE_MBTC) {
                val mBTCToSend = amtToSend
                amtToSend = mBTCToSend / 1000.0
                formattedStr = formatter.format(amtToSend)
            }

            if (displayUnits == MonetaryFormat.CODE_UBTC) {
                val uBTCToSend = amtToSend
                amtToSend = uBTCToSend / 1000000.0
                formattedStr = formatter.format(amtToSend)
            }

            if (displayUnits == "sats") {
                val satsToSend = amtToSend.toLong()
                amtToSend = satsToSend / 100000000.0
                formattedStr = formatter.format(amtToSend)
            }

            println(Coin.parseCoin(formattedStr!!).toPlainString())
            val amtCheckVal = java.lang.Double.parseDouble(Coin.parseCoin(formattedStr!!).toPlainString())

            if (TextUtils.isEmpty(uiHelper.recipient)) {
                uiHelper.showToastMessage("Please enter a recipient.")
            } else if (TextUtils.isEmpty(amount) || amtCheckVal < 0.0000001) {
                uiHelper.showToastMessage("Enter a valid amount. Minimum is 0.0000001 BCH")
            } else {
                //We call this section if the recipient entry is not a typical Bitcoin address like a 1 address or q (cashaddr) address.
                //Check if it's a BIP70 payment address.
                val receiverAddress = uiHelper.recipient
                if(receiverAddress.startsWith("+")) {
                    val numberString = receiverAddress.replace("+", "")
                    var amtToSats = java.lang.Double.parseDouble(formattedStr)
                    val satFormatter = DecimalFormat("#", symbols)
                    amtToSats *= 100000000
                    val sats = satFormatter.format(amtToSats).toInt()
                    val url = "https://pay.cointext.io/p/$numberString/$sats"
                    println(url)
                    val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(url)

                    val session = future.get()
                    if (session.isExpired) {
                        uiHelper.showToastMessage("Invoice expired!")
                    }

                    val req = session.sendRequest
                    wallet.completeTx(req)

                    val ack = session.sendPayment(ImmutableList.of(req.tx), wallet.freshReceiveAddress(), null)
                    if (ack != null) {
                        Futures.addCallback<PaymentProtocol.Ack>(ack, object : FutureCallback<PaymentProtocol.Ack> {
                            override fun onSuccess(ack: PaymentProtocol.Ack?) {
                                wallet.commitTx(req.tx)
                                MainActivity.INSTANCE.runOnUiThread {
                                    uiHelper.btnSend_AM.isEnabled = true
                                    uiHelper.sendUnit.text = displayUnits
                                    uiHelper.displayRecipientAddress(null)
                                }
                            }

                            override fun onFailure(throwable: Throwable) {

                            }
                        })
                    }

                } else if (receiverAddress.startsWith("https://")) {
                    val future: ListenableFuture<PaymentSession>

                    if (receiverAddress.startsWith("https")) {
                        try {
                            /*
                            As of April 25th, 2019 BitPay's cert expired for these invoices, so for the time being we do not verify pki.
                             */
                            future = PaymentSession.createFromUrl(receiverAddress)

                            val session = future.get()
                            if (session.isExpired) {
                                uiHelper.showToastMessage("Invoice expired!")
                            }

                            val req = session.sendRequest
                            wallet.completeTx(req)

                            val ack = session.sendPayment(ImmutableList.of(req.tx), wallet.freshReceiveAddress(), null)
                            if (ack != null) {
                                Futures.addCallback<PaymentProtocol.Ack>(ack, object : FutureCallback<PaymentProtocol.Ack> {
                                    override fun onSuccess(ack: PaymentProtocol.Ack?) {
                                        wallet.commitTx(req.tx)
                                        MainActivity.INSTANCE.runOnUiThread {
                                            uiHelper.btnSend_AM.isEnabled = true
                                            uiHelper.sendUnit.text = displayUnits
                                            uiHelper.displayRecipientAddress(null)
                                        }
                                    }

                                    override fun onFailure(throwable: Throwable) {

                                    }
                                })
                            }
                        } catch (e: PaymentProtocolException) {
                            e.printStackTrace()
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        } catch (e: ExecutionException) {
                            e.printStackTrace()
                        } catch (e: InsufficientMoneyException) {
                            uiHelper.showToastMessage("You do not have enough BCH!")
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                } else {
                    val toAddress = uiHelper.recipient

                    if (toAddress.contains("#")) {
                        val cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "")

                        if (toAddress != cashAccount) {
                            sendCoins(toAddress)
                        } else {
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("You cannot send to yourself!") }
                        }
                    } else {
                        try {
                            val isMyAddress = isAddressMine(uiHelper.recipient)

                            if (!isMyAddress) {
                                sendCoins(toAddress)
                            } else {
                                MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("You cannot send to yourself!") }
                            }
                        } catch (e: AddressFormatException) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Invalid address!") }
                        }

                    }
                }
            }
        }
    }

    private fun sendCoins(toAddress: String) {
        if (toAddress.contains("#") || Address.isValidCashAddr(parameters, toAddress) || Address.isValidLegacyAddress(parameters, toAddress)) {
            object : Thread() {
                override fun run() {
                    val recipientAddress = uiHelper.recipient
                    val coinAmt = Coin.parseCoin(formattedStr!!)

                    if (coinAmt.getValue() > 0.0) {
                        try {
                            val req: SendRequest
                            val cachedAddOpReturn = addOpReturn

                            if (coinAmt == getBalance(walletKit!!.wallet())) {
                                req = SendRequest.emptyWallet(parameters, recipientAddress)

                                /*
                                    Bitcoincashj requires emptying the wallet to only have a single output for some reason.
                                    So, we cache the original setting above, then set the real setting to false here.

                                    After doing the if(addOpReturn) check below, we restore it to its actual setting.
                                */
                                addOpReturn = false
                            } else {
                                req = SendRequest.to(parameters, recipientAddress, coinAmt)
                            }

                            req.ensureMinRequiredFee = false

                            req.feePerKb = Coin.valueOf(java.lang.Long.parseLong(1.toString() + "") * 1000L)

                            if (addOpReturn) {
                                val opReturnBytes = "Sent w/ crescent.cash!".toByteArray()
                                val script = ScriptBuilder.createOpReturnScript(opReturnBytes)

                                if (opReturnBytes.size <= Constants.MAX_OP_RETURN) {
                                    req.tx.addOutput(Coin.ZERO, script)
                                }
                            }

                            addOpReturn = cachedAddOpReturn
                            walletKit!!.wallet().sendCoins(walletKit!!.peerGroup(), req)
                        } catch (e: InsufficientMoneyException) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { e.message?.let { uiHelper.showToastMessage(it) } }
                        } catch (e: Wallet.CouldNotAdjustDownwards) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Not enough BCH for fee!") }
                        } catch (e: Wallet.ExceededMaxTransactionSize) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Transaction is too large!") }
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Cash Account not found.") }
                        }

                    }
                }
            }.start()
        } else if (!Address.isValidCashAddr(parameters, toAddress) || !Address.isValidLegacyAddress(parameters, toAddress)) {
            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Invalid address!") }
        }
    }

    private fun setupWalletListeners(wallet: Wallet) {
        wallet.addCoinsReceivedEventListener { wallet1, tx, prevBalance, newBalance ->
            if (!uiHelper.isDisplayingDownload) {
                uiHelper.displayMyBalance(getBalance(wallet).toFriendlyString())

                if (tx.purpose == Transaction.Purpose.UNKNOWN)
                    uiHelper.showToastMessage("Received coins!")

                val v = MainActivity.INSTANCE.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

                v.vibrate(100)

                getSLPWallet().refreshBalance()

                uiHelper.refresh()
            }
        }
        wallet.addCoinsSentEventListener { wallet12, tx, prevBalance, newBalance ->
            if (!uiHelper.isDisplayingDownload) {
                uiHelper.displayMyBalance(getBalance(wallet).toFriendlyString())
                uiHelper.clearAmount()
                uiHelper.displayRecipientAddress(null)
                uiHelper.showToastMessage("Sent coins!")
                uiHelper.sendSLPBtn.isEnabled = true
                uiHelper.btnSend_AM.isEnabled = true

                getSLPWallet().refreshBalance()

                uiHelper.refresh()
            }
        }
    }

    fun sendToken(tokenId: String, amount: BigDecimal, address: String) {
        uiHelper.sendSLPBtn.isEnabled = false
        object : Thread() {
            override fun run() {

                val trySend = getSLPWallet().sendToken(tokenId, amount, address)

                try {
                    trySend.subscribe(object : SingleObserver<String> {
                        override fun onSubscribe(d: Disposable) {

                        }

                        override fun onSuccess(s: String) {
                            getSLPWallet().refreshBalance()

                            MainActivity.INSTANCE.runOnUiThread {
                                uiHelper.sendSLPBtn.isEnabled = true
                                uiHelper.slpAmount.text = null
                                uiHelper.slpRecipientAddress.text = null
                                uiHelper.refresh()
                            }

                            getSLPWallet().clearSendStatus()
                        }

                        override fun onError(e: Throwable) {
                            getSLPWallet().refreshBalance()
                            MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Insufficient BCH balance.") }
                            getSLPWallet().clearSendStatus()
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }.start()
    }

    fun setupSLPWallet(seed: String?, hasSLPWallet: Boolean) {
        if (seed == null) {
            if (hasSLPWallet) {
                slpWallet = SLPWallet.getInstance(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST)
                MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()
            } else {
                val seedFromWallet = wallet.keyChainSeed
                val mnemonicCode = seedFromWallet.mnemonicCode
                val recoverySeed = StringBuilder()

                assert(mnemonicCode != null)
                for (x in mnemonicCode!!.indices) {
                    recoverySeed.append(mnemonicCode[x]).append(if (x == mnemonicCode.size - 1) "" else " ")
                }

                slpWallet = SLPWallet.fromMnemonic(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, recoverySeed.toString(), true)

                MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()

                MainActivity.INSTANCE.runOnUiThread { uiHelper.refresh() }
            }
        } else {
            slpWallet = SLPWallet.fromMnemonic(MainActivity.INSTANCE, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, seed, true)
            MainActivity.INSTANCE.prefs.edit().putBoolean("hasSLPWallet", true).apply()
        }

        getSLPWallet().balance.observe(MainActivity.INSTANCE, Observer { data -> balances = data })
    }

    fun getXPUB(): String {
        return serializedXpub
    }

    companion object {
        var addOpReturn: Boolean = false
    }
}
