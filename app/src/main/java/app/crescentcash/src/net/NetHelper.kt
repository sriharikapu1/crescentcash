package app.crescentcash.src.net

import android.os.CountDownTimer
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog

import com.google.common.base.Splitter

import org.bitcoinj.crypto.MnemonicCode
import org.bitcoinj.crypto.MnemonicException
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.wallet.DeterministicSeed
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLDecoder
import java.nio.charset.Charset
import java.security.SecureRandom
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.HashMap
import java.util.Random

import app.crescentcash.src.MainActivity
import app.crescentcash.src.R
import app.crescentcash.src.hash.HashHelper
import app.crescentcash.src.json.JSONHelper
import app.crescentcash.src.ui.UIHelper
import app.crescentcash.src.utils.Constants

class NetHelper {

    private val uiHelper: UIHelper = MainActivity.INSTANCE.uiHelper
    private val cashAcctServers = arrayOf("https://cashacct.imaginary.cash", "https://api.cashaccount.info")

    private val blockExplorers = arrayOf("btc.com", "blockdozer.com", "coin.space")

    private val blockExplorerAPIURL = arrayOf("https://bch-chain.api.btc.com/v3/tx/", "https://blockdozer.com/api/tx/", "https://bch.coin.space/api/tx/")

    private val entropy: ByteArray
        get() = getEntropy(SecureRandom())

    val price: Double
        get() {
            val url = "https://api.coinmarketcap.com/v1/ticker/" + "bitcoin-cash" + "/"

            var price = 0.0
            var `is`: InputStream? = null
            try {
                `is` = URL(url).openStream()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            try {
                val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
                val jsonText = JSONHelper.readJSONFile(rd)
                val json = JSONArray(jsonText)
                val priceStr = json.getJSONObject(0).getString("price_usd")

                price = java.lang.Double.parseDouble(priceStr)
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                try {
                    `is`?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            return price
        }

    fun prepareWalletForRegistration() {
        val cashAcctName = uiHelper.handle.text.toString()

        if (!TextUtils.isEmpty(cashAcctName)) {
            if (!cashAcctName.contains("#") && !cashAcctName.contains(".")) {
                val entropy = entropy
                var mnemonic: List<String>? = null
                try {
                    mnemonic = MnemonicCode.INSTANCE.toMnemonic(entropy)
                } catch (e: MnemonicException.MnemonicLengthException) {
                    e.printStackTrace()
                }

                val mnemonicCode = mnemonic
                val recoverySeed = StringBuilder()

                assert(mnemonicCode != null)
                for (x in mnemonicCode!!.indices) {
                    recoverySeed.append(mnemonicCode[x]).append(if (x == mnemonicCode.size - 1) "" else " ")
                }

                val seedStr = recoverySeed.toString()

                val creationTime = 1551084297L
                val seed = DeterministicSeed(Splitter.on(' ').splitToList(seedStr), null, "", creationTime)

                val length = Splitter.on(' ').splitToList(seedStr).size

                if (length == 12) {
                    val walletHelper = MainActivity.INSTANCE.walletHelper
                    walletHelper.setupSLPWallet(seedStr, true)
                    walletHelper.setupWalletKit(seed, uiHelper.handle.text.toString(), false)
                    uiHelper.displayDownloadContent(true)
                    uiHelper.new_wallet.visibility = View.GONE
                    uiHelper.showToastMessage("Registering user...")

                    walletHelper.timer = object : CountDownTimer(150000, 20) {

                        override fun onTick(millisUntilFinished: Long) {

                        }

                        override fun onFinish() {
                            try {
                                checkForAccountIdentity(uiHelper.handle.text.toString())
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }.start()
                }
            } else {
                uiHelper.showToastMessage("Do not include identifier!")
            }
        }

        uiHelper.hideKeyboard(MainActivity.INSTANCE)
    }

    fun registerCashAccount(cashAcctName: String, walletAddress: String) {
        object : Thread() {
            override fun run() {
                if (!cashAcctName.contains("#")) {
                    val json = JSONObject()

                    val walletHelper = MainActivity.INSTANCE.walletHelper

                    try {
                        json.put("name", cashAcctName)

                        val paymentsArray = JSONArray()
                        paymentsArray.put(walletAddress)

                        json.put("payments", paymentsArray)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    val requestUrl = "https://api.cashaccount.info/register/"
                    var url: URL? = null
                    try {
                        url = URL(requestUrl)
                    } catch (e: MalformedURLException) {
                        e.printStackTrace()
                    }

                    var connection: HttpURLConnection? = null

                    try {
                        if (url != null) {
                            connection = url.openConnection() as HttpURLConnection
                        }
                        if (connection != null) {
                            connection.doOutput = true
                            connection.doInput = true
                            connection.instanceFollowRedirects = false
                            connection.requestMethod = "POST"
                            connection.setRequestProperty("Content-Type", "application/json")
                            connection.setRequestProperty("charset", "utf-8")
                            connection.setRequestProperty("Accept", "application/json")
                            connection.setRequestProperty("Content-Length", Integer.toString(json.toString().toByteArray().size))
                            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0")

                            connection.useCaches = false

                            connection.connectTimeout = 60000
                            connection.readTimeout = 60000

                            connection.connect()

                            val wr = DataOutputStream(connection.outputStream)
                            wr.write(json.toString().toByteArray())
                            wr.flush()
                            wr.close()

                            val rd = BufferedReader(InputStreamReader(connection.inputStream))
                            val res = StringBuilder()
                            while (true) {
                                val line = rd.readLine()

                                if(line != null)
                                    res.append(line)
                                else
                                    break
                            }

                            wr.flush()
                            wr.close()


                            val responseJson = res.toString()
                            println(responseJson)

                            val jsonHelper = JSONHelper()

                            walletHelper.registeredTxHash = jsonHelper.getRegisterTxHash(responseJson)
                            System.out.println(walletHelper.registeredTxHash)
                            val editor = MainActivity.INSTANCE.prefs.edit()
                            editor.putString("cashAccount", "$cashAcctName#???")
                            editor.putString("cashEmoji", "?")
                            editor.putString("cashAcctTx", walletHelper.registeredTxHash)
                            editor.apply()
                            MainActivity.INSTANCE.runOnUiThread {

                                val builder = AlertDialog.Builder(MainActivity.INSTANCE, if (UIHelper.nightModeEnabled) R.style.AlertDialogDark else R.style.AlertDialogLight)
                                builder.setTitle("WARNING!")
                                builder.setMessage("Be sure to write down your recovery seed and save your Cash Account name and identifier when confirmed! You will need it to restore the wallet in this app!")
                                builder.setCancelable(true)
                                builder.setPositiveButton("Hide") { dialog, which -> dialog.dismiss() }
                                val alertDialog = builder.create()
                                alertDialog.show()
                                val msgTxt = alertDialog.findViewById<View>(android.R.id.message) as TextView
                                msgTxt.movementMethod = LinkMovementMethod.getInstance()

                                uiHelper.displayCashAccount("$cashAcctName#???")
                                uiHelper.myEmoji.text = "?"
                                uiHelper.showToastMessage("Registered!")
                                uiHelper.refresh()
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    MainActivity.INSTANCE.runOnUiThread { uiHelper.showToastMessage("Do not set an account number!") }
                }

            }
        }.start()

    }


    fun prepareWalletForVerification() {
        val cashAcctName = uiHelper.handle2.text.toString()

        if (!TextUtils.isEmpty(cashAcctName)) {
            if (cashAcctName.contains("#")) {
                val seedStr = uiHelper.recoverySeed.text.toString()

                val creationTime = 1551084297L
                val seed = DeterministicSeed(Splitter.on(' ').splitToList(seedStr), null, "", creationTime)

                val length = Splitter.on(' ').splitToList(seedStr).size

                if (length == 12) {
                    val walletHelper = MainActivity.INSTANCE.walletHelper
                    walletHelper.setupSLPWallet(seedStr, true)
                    MainActivity.INSTANCE.prefs.edit().putBoolean("isNewUser", false).apply()
                    walletHelper.setupWalletKit(seed, cashAcctName, true)
                    uiHelper.showToastMessage("Verifying user...")
                }
            } else {
                uiHelper.showToastMessage("Please include the identifier!")
            }
        }

        uiHelper.hideKeyboard(MainActivity.INSTANCE)
    }

    fun getCashAccountEmoji(cashAccount: String): String {
        val randExplorer = Random().nextInt(cashAcctServers.size)
        val lookupServer = cashAcctServers[randExplorer]

        var emoji = ""

        val splitAccount = cashAccount.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val name = splitAccount[0]
        val block = splitAccount[1]

        if (!block.contains(".")) {

            var `is`: InputStream? = null
            try {
                `is` = URL("$lookupServer/account/$block/$name").openStream()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            try {
                val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
                val jsonText = JSONHelper.readJSONFile(rd)
                val json = JSONObject(jsonText)
                emoji = json.getJSONObject("information").getString("emoji")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                try {
                    `is`?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        } else {
            val splitBlock = block.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val mainBlock = splitBlock[0]
            val blockCollision = splitBlock[1]


            var `is`: InputStream? = null
            try {
                `is` = URL("$lookupServer/account/$mainBlock/$name/$blockCollision").openStream()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            try {
                val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
                val jsonText = JSONHelper.readJSONFile(rd)
                val json = JSONObject(jsonText)
                emoji = json.getJSONObject("information").getString("emoji")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                try {
                    `is`?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
        return emoji
    }

    fun initialAccountIdentityCheck(name: String) {
        object : Thread() {
            override fun run() {
                val walletHelper = MainActivity.INSTANCE.walletHelper

                if (walletHelper.registeredBlock == "null" || walletHelper.registeredBlock == "???") {
                    when {
                        walletHelper.registeredTxHash == "null" -> println("No response from registration server...")
                        walletHelper.registeredTxHash != "" -> try {
                            walletHelper.registeredBlock = getTransactionsBlock(walletHelper.registeredTxHash)

                            if (walletHelper.registeredBlock == "???") {
                                println("Block not found... checking in 2.5 minutes.")
                            } else if (walletHelper.registeredBlock != "") {
                                walletHelper.registeredBlockHash = getTransactionsBlockHash(walletHelper.registeredTxHash)
                                val accountIdentity = Integer.parseInt(walletHelper.registeredBlock) - Constants.CASH_ACCOUNT_GENESIS_MODIFIED

                                val hashHelper = HashHelper()
                                System.out.println("Block hash: " + walletHelper.registeredBlockHash)
                                System.out.println("Block tx hash: " + walletHelper.registeredTxHash)
                                val collisionIdentifier = hashHelper.getCashAccountCollision(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                println("$name#$accountIdentity.$collisionIdentifier")
                                val identifier = getCashAccountIdentifier("$name#$accountIdentity.$collisionIdentifier")
                                val emoji = hashHelper.getCashAccountEmoji(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                MainActivity.INSTANCE.prefs.edit().putString("cashAccount", identifier).apply()
                                MainActivity.INSTANCE.prefs.edit().putString("cashEmoji", emoji).apply()
                                println(identifier)
                                println(emoji)

                                if (!uiHelper.isDisplayingDownload && MainActivity.INSTANCE.walletHelper.walletKit != null)
                                    MainActivity.INSTANCE.runOnUiThread { uiHelper.refresh() }
                            }
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                        else -> println("Error: No transaction hash found.")
                    }
                }
            }
        }.start()
    }

    fun checkForAccountIdentity(name: String) {
        object : Thread() {
            override fun run() {
                val walletHelper = MainActivity.INSTANCE.walletHelper
                if (walletHelper.registeredBlock == "null" || walletHelper.registeredBlock == "???") {
                    when {
                        walletHelper.registeredTxHash == "null" -> {
                            println("No response from registration server...")
                            walletHelper.timer.start()
                        }
                        walletHelper.registeredTxHash != "" -> {
                            val hashHelper = HashHelper()

                            try {
                                walletHelper.registeredBlock = getTransactionsBlock(walletHelper.registeredTxHash)

                                if (walletHelper.registeredBlock == "???") {
                                    println("Block not found... checking in 2.5 minutes.")
                                    walletHelper.timer.start()
                                } else if (walletHelper.registeredBlock != "") {
                                    walletHelper.registeredBlockHash = getTransactionsBlockHash(walletHelper.registeredTxHash)
                                    val accountIdentity = Integer.parseInt(walletHelper.registeredBlock) - Constants.CASH_ACCOUNT_GENESIS_MODIFIED
                                    val collisionIdentifier = hashHelper.getCashAccountCollision(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                    val identifier = getCashAccountIdentifier("$name#$accountIdentity.$collisionIdentifier")
                                    val emoji = hashHelper.getCashAccountEmoji(walletHelper.registeredBlockHash, walletHelper.registeredTxHash)
                                    MainActivity.INSTANCE.prefs.edit().putString("cashAccount", identifier).apply()
                                    MainActivity.INSTANCE.prefs.edit().putString("cashEmoji", emoji).apply()
                                    println(identifier)
                                    println(emoji)

                                    if (!uiHelper.isDisplayingDownload && MainActivity.INSTANCE.walletHelper.walletKit != null)
                                        MainActivity.INSTANCE.runOnUiThread { uiHelper.refresh() }
                                }
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            }

                        }
                        else -> {
                            println("Error: No transaction hash found.")
                            walletHelper.timer.start()
                        }
                    }
                }
            }
        }.start()
    }

    private fun getCashAccountIdentifier(cashAccount: String): String {
        val randExplorer = Random().nextInt(cashAcctServers.size)
        val lookupServer = cashAcctServers[randExplorer]

        var identity = ""

        val splitAccount = cashAccount.split("#".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val name = splitAccount[0]
        val block = splitAccount[1]

        val splitBlock = block.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val mainBlock = splitBlock[0]
        val blockCollision = splitBlock[1]


        var `is`: InputStream? = null
        try {
            `is` = URL("$lookupServer/account/$mainBlock/$name/$blockCollision").openStream()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        try {
            val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
            val jsonText = JSONHelper.readJSONFile(rd)
            val json = JSONObject(jsonText)
            identity = json.getString("identifier").replace(";", "")
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return identity
    }

    private fun getTransactionsBlock(transactionHash: String): String {
        val randExplorer = Random().nextInt(blockExplorers.size)
        val blockExplorer = blockExplorers[randExplorer]
        val blockExplorerURL = blockExplorerAPIURL[randExplorer]

        var block = ""
        val txHash = transactionHash.toLowerCase()
        var `is`: InputStream? = null
        try {
            `is` = URL(blockExplorerURL + txHash).openStream()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        try {
            val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
            val jsonText = JSONHelper.readJSONFile(rd)
            val json = JSONObject(jsonText)

            if (blockExplorer == "btc.com") {
                block = json.getJSONObject("data").getString("block_height")
            } else if (blockExplorer == "blockdozer.com" || blockExplorer == "coin.space") {
                block = json.getString("blockheight")
            }

        } catch (e: JSONException) {
            e.printStackTrace()
            return "???"
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return if (block == "-1") "???" else block
    }

    fun getTransactionsBlockHash(transactionHash: String): String {
        val randExplorer = Random().nextInt(blockExplorers.size)
        val blockExplorer = blockExplorers[randExplorer]
        val blockExplorerURL = blockExplorerAPIURL[randExplorer]

        var block = ""
        val txHash = transactionHash.toLowerCase()
        var `is`: InputStream? = null
        try {
            `is` = URL(blockExplorerURL + txHash).openStream()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        try {
            val rd = BufferedReader(InputStreamReader(`is`, Charset.forName("UTF-8")))
            val jsonText = JSONHelper.readJSONFile(rd)
            val json = JSONObject(jsonText)

            if (blockExplorer == "btc.com") {
                block = json.getJSONObject("data").getString("block_hash")
            } else if (blockExplorer == "blockdozer.com" || blockExplorer == "coin.space") {
                block = json.getString("blockhash")
            }

        } catch (e: JSONException) {
            block = "???"
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return if (block == "-1") "???" else block
    }

    private fun getEntropy(random: SecureRandom): ByteArray {
        val seed = ByteArray(DeterministicSeed.DEFAULT_SEED_ENTROPY_BITS / 8)
        random.nextBytes(seed)
        return seed
    }

    fun handleURI(address: String) {
        println("This is a URI. Getting variables.")
        val mappedVariables = getQueryParams(address)
        println("All variables $mappedVariables")
        this.uiHelper.displayRecipientAddress(getQueryBaseAddress(address))
        val amount = (mappedVariables["amount"] ?: error(""))[0]
        val df = DecimalFormat("#,###.########")

        when (MainActivity.INSTANCE.walletHelper.displayUnits) {
            MonetaryFormat.CODE_BTC -> {
                uiHelper.amountText.text = df.format(java.lang.Double.parseDouble(amount)).replace(",", "")
                println("Amount to pay: " + df.format(java.lang.Double.parseDouble(amount)))
            }
            MonetaryFormat.CODE_MBTC -> {
                var amt = java.lang.Double.parseDouble(amount)
                amt *= 1000.0
                uiHelper.amountText.text = df.format(amt).replace(",", "")
                println("Amount to pay: " + df.format(amt))
            }
            MonetaryFormat.CODE_UBTC -> {
                var amt = java.lang.Double.parseDouble(amount)
                amt *= 1000000.0
                uiHelper.amountText.text = df.format(amt).replace(",", "")
                println("Amount to pay: " + df.format(amt))
            }
            "sats" -> {
                var amt = java.lang.Double.parseDouble(amount)
                amt *= 100000000.0
                uiHelper.amountText.text = df.format(amt).replace(",", "")
                println("Amount to pay: " + df.format(amt))
            }
        }
    }

    private fun getQueryParams(url: String): Map<String, List<String>> {
        try {
            val params = HashMap<String, List<String>>()
            val urlParts = url.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (urlParts.size > 1) {
                val query = urlParts[1]
                for (param in query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                    val pair = param.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val key = URLDecoder.decode(pair[0], "UTF-8")
                    var value = ""
                    if (pair.size > 1) {
                        value = URLDecoder.decode(pair[1], "UTF-8")
                    }

                    var values: MutableList<String>? = params[key] as MutableList<String>?
                    if (values == null) {
                        values = ArrayList()
                        params[key] = values
                    }
                    values.add(value)
                }
            }

            return params
        } catch (ex: UnsupportedEncodingException) {
            throw AssertionError(ex)
        }

    }

    private fun getQueryBaseAddress(url: String): String {
        val urlParts = url.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (urlParts.size > 1) {
            urlParts[0]
        } else {
            "no address"
        }
    }
}